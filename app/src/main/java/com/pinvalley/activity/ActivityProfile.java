package com.pinvalley.activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.pinvalley.R;
import com.pinvalley.utils.Utils;

public class ActivityProfile extends ActivityPVBase implements View.OnClickListener {

    private Button btn_logout;
    ImageView header_pic;
    TextView name, email;
    TextView email_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setUpToolbar(null);

        email_text = (TextView) findViewById(R.id.email_text);

        name = (TextView) findViewById(R.id.name);
        email = (TextView) findViewById(R.id.email);


        header_pic = (ImageView) findViewById(R.id.header_pic);

        SharedPreferences AssetPref = getSharedPreferences(Utils.PREFERENCE_ASSET_VAL, 0);
        String name = AssetPref.getString(Utils.PREF_USER_NAME, "");
        String email = AssetPref.getString(Utils.PREF_USER_EMAIL, "");

        this.name.setText(name);
        this.email.setText(email);

        name = name.trim();
        String initials;
        if(name.contains(" ")) {
            String[] names = name.split(" ");
            initials = String.valueOf(names[0].charAt(0)) + String.valueOf(names[1].charAt(0));
        } else{
            initials = String.valueOf(name.charAt(0));
        }
        TextDrawable nameDrawable = TextDrawable.builder()
                .buildRound(initials, Color.BLACK);
        header_pic.setImageDrawable(nameDrawable);
    }
    @Override
    public void onClick(View v) {

    }

}