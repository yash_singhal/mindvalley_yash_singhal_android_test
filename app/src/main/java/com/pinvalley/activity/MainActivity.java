package com.pinvalley.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.pinvalley.R;
import com.pinvalley.adapters.PinsAdapter;
import com.pinvalley.fragment.FragmentDrawer;
import com.pinvalley.model.response.PinResponseItem;
import com.pinvalley.services.PVServiceGenerator;
import com.pinvalley.services.api.PinValleyAPI;
import com.pinvalley.utils.Utils;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FragmentDrawer.FragmentDrawerListener, Callback<ArrayList<PinResponseItem>> {

    private FragmentDrawer drawerFragment;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    Toolbar mToolbar;
    RecyclerView recyclerView;
    PinsAdapter pinsAdapter;
    ArrayList<PinResponseItem> pins;

    PinsAdapter.OnItemClickListener onItemClickListener = new PinsAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {
            Intent pinDetailIntent = new Intent(MainActivity.this, ActivityPinDetails.class);

            try {
                Bundle bundle = new Bundle();
                bundle.putInt(Utils.PARAM_SELECTED_INDEX, position);
                bundle.putString(Utils.PARAM_SELECTED_PIN_IMAGE, pins.get(position).getUrls().getRegular());
                bundle.putString(Utils.PARAM_SELECTED_PIN_CREATED_AT, pins.get(position).getCreated_at());
                bundle.putBoolean(Utils.PARAM_SELECTED_PIN_LIKED_BY_USER, pins.get(position).getLiked_by_user());
                bundle.putInt(Utils.PARAM_SELECTED_PIN_LIKES, pins.get(position).getLikes());
                pinDetailIntent.putExtras(bundle);

                startActivityForResult(pinDetailIntent, Utils.PIN_DETAIL_ACTIVITY);

                MainActivity.this.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            } catch (IndexOutOfBoundsException ex){
                ex.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        initializeDrawer();

        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, 1));
        recyclerView.setHasFixedSize(true);
        PinValleyAPI pinValleyAPI = PVServiceGenerator.createService(PinValleyAPI.class, true);
        Call<ArrayList<PinResponseItem>> call = pinValleyAPI.getPins(Utils.BASE_URL);
        call.enqueue(this);
    }

    private void initializeDrawer() {
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
       switch (position){
           case 3:{
               invokeProfile();
           }
           break;
       }

    }

    private void invokeProfile(){
        Intent profileIntent = new Intent(MainActivity.this, ActivityProfile.class);
        startActivityForResult(profileIntent, Utils.NAV_PROFILE_ACTIVITY);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResponse(Response<ArrayList<PinResponseItem>> response, Retrofit retrofit) {
        pins = response.body();
        if(pins!= null && pins.size() > 0){
            pinsAdapter = new PinsAdapter(this, pins);
            pinsAdapter.setOnItemClickListener(onItemClickListener);
            recyclerView.setAdapter(pinsAdapter);
        }
    }

    @Override
    public void onFailure(Throwable t) {
        t.printStackTrace();
    }
}
