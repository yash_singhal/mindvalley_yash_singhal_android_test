package com.pinvalley.activity;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.pinvalley.R;

public class ActivityPVBase extends AppCompatActivity {

    ProgressDialog pd;

    public void setUpToolbar(String title) {

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_action);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        if (title != null) {
            actionBar.setTitle(title);
        }
    }

    public void titleToolbar(){
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
    }

    public void showProgress(String message, boolean cancelable, boolean cancelableTouch) {
        if (!isFinishing()) {
            pd = new ProgressDialog(ActivityPVBase.this);
            pd.setMessage(message);
            pd.setCancelable(cancelable);
            pd.setCanceledOnTouchOutside(cancelableTouch);
            pd.show();
        }
    }

    public void hideProgress() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (pd != null && pd.isShowing())
                    pd.dismiss();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}
