package com.pinvalley.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pinvalley.R;
import com.pinvalley.utils.Utils;
import com.squareup.picasso.Picasso;

/**
 * Created by yashsinghal on 18/09/16.
 */
public class ActivityPinDetails extends ActivityPVBase implements View.OnClickListener {
    ImageView pin_image;
    TextView created_at, likes_text, liked_by_user;
    private FloatingActionButton like_button;
    int likes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_details);
        setUpToolbar(null);
        pin_image = (ImageView) findViewById(R.id.pin_image);
        created_at = (TextView) findViewById(R.id.created_at);
        likes_text = (TextView) findViewById(R.id.likes);
        liked_by_user = (TextView) findViewById(R.id.liked_by_user);
        like_button = (FloatingActionButton) findViewById(R.id.fab);
        like_button.setOnClickListener(this);


    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle bundle = this.getIntent().getExtras();
        if(bundle != null){
            String pin_image = bundle.getString(Utils.PARAM_SELECTED_PIN_IMAGE);
            String[] date_array = bundle.getString(Utils.PARAM_SELECTED_PIN_CREATED_AT).split("-");
            likes= bundle.getInt(Utils.PARAM_SELECTED_PIN_LIKES);
            boolean liked_by_user = bundle.getBoolean(Utils.PARAM_SELECTED_PIN_LIKED_BY_USER);

            String imageURL = null;
            try{
                imageURL = pin_image;

            }catch (Exception ex){

            }

            Picasso.with(this)
                    .load(imageURL)
                    .placeholder(R.drawable.feed_default_img)
                    .error(R.drawable.feed_default_img)
                    .into(this.pin_image);

            StringBuilder created_at = new StringBuilder();
            created_at.append(date_array[0]+"/");
            created_at.append(date_array[1]+"/");
            created_at.append(date_array[2].substring(0, date_array[2].indexOf("T")));

            this.created_at.setText(created_at);

            likes_text.setText("Likes:"+String.valueOf(likes));
            if(liked_by_user){
                this.liked_by_user.setText("Liked By User");
            } else{
                this.liked_by_user.setText("Not Liked By User");
            }
        }
    }

    @Override
    public void onClick(View v) {
        likes = likes + 1;
        likes_text.setText("Likes:"+String.valueOf(likes));
        this.liked_by_user.setText("Liked By User");

    }
}
