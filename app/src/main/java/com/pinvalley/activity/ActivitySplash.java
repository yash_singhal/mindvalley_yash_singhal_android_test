package com.pinvalley.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.style.Wave;
import com.pinvalley.R;
import com.pinvalley.utils.Utils;


public class ActivitySplash extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash);

        SharedPreferences AssetPref = getSharedPreferences(Utils.PREFERENCE_ASSET_VAL, 0);
        SharedPreferences.Editor editor = AssetPref.edit();
        String name = AssetPref.getString(Utils.PREF_USER_NAME, null);
        if(name == null) {
            editor.putString(Utils.PREF_USER_NAME, getResources().getString(R.string.user_name));
            editor.putString(Utils.PREF_USER_EMAIL, getResources().getString(R.string.user_email));
            editor.apply();
        }

        ProgressBar progressBar = (ProgressBar)findViewById(R.id.spin_kit);
        Wave wave = new Wave();
        progressBar.setIndeterminateDrawable(wave);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    Intent i = new Intent(ActivitySplash.this, MainActivity.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                    finish();

            }
        }, 2000);


    }

}
