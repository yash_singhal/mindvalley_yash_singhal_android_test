package com.pinvalley.services.api;

import com.pinvalley.model.response.PinResponseItem;
import com.pinvalley.utils.Utils;
import com.squareup.okhttp.ResponseBody;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import retrofit.http.Url;

/**
 * Created by gyanendrasingh on 1/4/16.
 */
public interface PinValleyAPI {


//////////////////////////////////////////////////////// AWS set up//////////////////////////////////////////////////////////////////////////

    @GET
    Call<ArrayList<PinResponseItem>> getPins(@Url String url);


}
