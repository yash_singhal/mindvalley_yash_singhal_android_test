package com.pinvalley.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pinvalley.utils.Utils;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by gyanendrasingh on 1/5/16.
 */
public class PVServiceGenerator {

    public static final String API_BASE_URL = Utils.BASE_URL;

    private static OkHttpClient httpClient = new OkHttpClient();

    private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

    private static Gson gson = new GsonBuilder().create();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson));

    public static <S> S createService(Class<S> serviceClass, boolean enableLogging) {
        if (enableLogging) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.interceptors().add(logging);  // <-- this is the important line!
        }

        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }
}
