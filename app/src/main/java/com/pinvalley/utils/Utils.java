package com.pinvalley.utils;

/**
 * Created by gyanendrasingh on 12/22/15.
 */
public class Utils {


    //Constants-values


    //Nav Related
    public static final int NAV_PROFILE_ACTIVITY = 55;
    public static final int PIN_DETAIL_ACTIVITY=68;

    public static final String PARAM_SELECTED_INDEX = "selected_index";
    public static final String PARAM_SELECTED_PIN = "pin_obj";
    public static final String PARAM_SELECTED_PIN_NAME = "pin_name";
    public static final String PARAM_SELECTED_PIN_URL = "property_url";
    public static final String PARAM_SELECTED_PIN_IMAGE = "pin_image";
    public static final String PARAM_SELECTED_PIN_CREATED_AT = "pin_created_at";
    public static final String PARAM_SELECTED_PIN_LIKES = "pin_likes";
    public static final String PARAM_SELECTED_PIN_LIKED_BY_USER = "pin_liked_by_user";


    // ----------------------------------------------------------------
    // ... Retro Fit Related
    // ----------------------------------------------------------------
    //APIS
    public static final String BASE_URL = " http://pastebin.com/raw/wgkJgazE";


    // ----------------------------------------------------------------
    // ... Shared Preference Keys
    // ----------------------------------------------------------------
    public static final String PREFERENCE_ASSET_VAL = "ASSET_VAL";

    //For session
    public static final String PREF_USER_AT = "PREF_USER_AT";
    public static final String PREF_USER_NAME = "PREF_USER_NAME";
    public static final String PREF_USER_EMAIL = "PREF_USER_EMAIL";

    //Device id
    public static final String PREF_DEVICE_ID = "PREF_DEVICE_ID";

}
