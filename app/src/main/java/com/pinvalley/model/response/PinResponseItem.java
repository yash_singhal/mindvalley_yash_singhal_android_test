package com.pinvalley.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.pinvalley.model.Urls;

/**
 * Created by yashsinghal on 17/09/16.
 */
public class PinResponseItem implements Parcelable {

    public static final Parcelable.Creator<PinResponseItem> CREATOR = new Parcelable.Creator<PinResponseItem>() {
        public PinResponseItem createFromParcel(Parcel in) {
            return new PinResponseItem(in);
        }

        public PinResponseItem[] newArray(int size) {
            return new PinResponseItem[size];
        }
    };
    private String created_at;
    private int likes;
    private boolean liked_by_user;
    private Urls urls;

    //Parcelling part
    public PinResponseItem() {
        super();
        // TODO Auto-generated constructor stub
    }

    public PinResponseItem(Parcel in) {
        this();
        readFromParcel(in);
    }

    public String getCreated_at() {
        return created_at;
    }

    public int getLikes() {
        return likes;
    }

    public boolean getLiked_by_user() {
        return liked_by_user;
    }

    public Urls getUrls() {
        return urls;
    }

    private void readFromParcel(Parcel in) {
        created_at = in.readString();
        likes = in.readInt();
        liked_by_user = in.readByte() != 0;
        urls = in.readParcelable(getClass().getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(created_at);
        dest.writeInt(likes);
        dest.writeByte((byte) (liked_by_user ? 1 : 0));
        dest.writeParcelable(urls, flags);

    }

}
