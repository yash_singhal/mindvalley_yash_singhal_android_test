package com.pinvalley.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by yashsinghal on 17/09/16.
 */
public class Urls implements Parcelable {

    public static final Parcelable.Creator<Urls> CREATOR = new Parcelable.Creator<Urls>() {
        public Urls createFromParcel(Parcel in) {
            return new Urls(in);
        }

        public Urls[] newArray(int size) {
            return new Urls[size];
        }
    };
    private String full;
    private String thumb;
    private String small;
    private String regular;


    public Urls() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Urls(Parcel in) {
        this();
        readFromParcel(in);
    }

    public String getFull() {
        return full;
    }

    public String getThumb() {
        return thumb;
    }

    public String getSmall() {
        return small;
    }

    public String getRegular() {
        return regular;
    }

    private void readFromParcel(Parcel in) {
        this.full = in.readString();
        this.thumb = in.readString();
        this.small = in.readString();
        this.regular = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(full);
        dest.writeString(thumb);
        dest.writeString(small);
        dest.writeString(regular);
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }
}
