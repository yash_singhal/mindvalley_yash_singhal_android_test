package com.pinvalley.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pinvalley.R;
import com.pinvalley.model.response.PinResponseItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashsinghal on 17/09/16.
 */
public class PinsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    Context mContext;
    List<PinResponseItem> pins;
    OnItemClickListener mItemClickListener;

    public PinsAdapter(Context context, List<PinResponseItem> pins) {
        this.mContext = context;
        this.pins = pins;
    }


    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pin_item_layout, parent, false);
            vh = new ViewHolder(view);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progess_bar, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {

            final ViewHolder viewHolder = (ViewHolder) holder;
            final PinResponseItem pin = pins.get(position);
            String imageURL = null;
            try{
                imageURL = pin.getUrls().getSmall();

            }catch (Exception ex){

            }

            Picasso.with(mContext)
                    .load(imageURL)
                    .placeholder(R.drawable.feed_default_img)
                    .error(R.drawable.feed_default_img)
                    .into(viewHolder.pin_image);

            viewHolder.likes.setText("Likes:"+String.valueOf(pin.getLikes()));
            if(pin.getLiked_by_user()){
                viewHolder.liked_by_user.setText("Liked By User");
            } else{
                viewHolder.liked_by_user.setText("Not Liked By User");
            }
            String[] date_array = pin.getCreated_at().split("-");
            StringBuilder created_at = new StringBuilder();
            created_at.append(date_array[0]+"/");
            created_at.append(date_array[1]+"/");
            created_at.append(date_array[2].substring(0, date_array[2].indexOf("T")));

            viewHolder.created_at.setText(created_at);

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return pins.size();
    }

    @Override
    public int getItemViewType(int position) {
        return pins.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.VISIBLE);
        }
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView pin_image;
        TextView created_at;
        TextView likes;
        TextView liked_by_user;
        LinearLayout ripple_layout;
        public ViewHolder(View itemView) {
            super(itemView);
            pin_image = (ImageView) itemView.findViewById(R.id.pin_image);
            created_at = (TextView) itemView.findViewById(R.id.created_at);
            likes = (TextView) itemView.findViewById(R.id.likes);
            liked_by_user = (TextView) itemView.findViewById(R.id.liked_by_user);
            ripple_layout = (LinearLayout) itemView.findViewById(R.id.ripple_layout);

            ripple_layout.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(itemView, getPosition());
            }

        }
    }
}
