package com.pinvalley.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.pinvalley.R;
import com.pinvalley.adapters.NavigationDrawerAdapter;
import com.pinvalley.utils.NavDrawerItem;
import com.pinvalley.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashsinghal on 11/03/16.
 */
public class FragmentDrawer extends Fragment {

    public static String TAG = FragmentDrawer.class.getSimpleName();
    private RecyclerView recyclerView;
    public ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerAdapter adapter;
    private View containerView;
    private static String[] titles = null;
    private static TypedArray navMenuIcons;
    private FragmentDrawerListener drawerListener;
    private ImageView img;
    final String SEND_PHOTO_INTENT = "PHOTO_INTENT";
    TextView profile_name;
    TextView profile_email;
    String userAt;
    LinearLayout logged_in;
    ImageView header_image;
    SharedPreferences AssetPref;
    public FragmentDrawer() {

    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;

    }

    public static List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();


        // preparing navigation drawer items
        for (int i = 0; i < titles.length ; i++) {
            NavDrawerItem navItem = new NavDrawerItem();
            navItem.setTitle(titles[i]);
            data.add(navItem);
        }
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
        header_image = (ImageView) layout.findViewById(R.id.header_pic) ;
        logged_in = (LinearLayout) layout.findViewById(R.id.logged_in_header);
        profile_name = (TextView) layout.findViewById(R.id.profile_name);
        profile_email = (TextView) layout.findViewById(R.id.profile_email);

        return layout;
    }

    private void initializeView() {
        String name = "";
        String email = "";
        AssetPref = getActivity().getSharedPreferences(Utils.PREFERENCE_ASSET_VAL, 0);
        String user_name = AssetPref.getString(Utils.PREF_USER_NAME, null);
        String user_email = AssetPref.getString(Utils.PREF_USER_EMAIL, null);
        if (user_email != null){
            email = user_email;
        }
        if(user_name != null) {
            name = user_name;
        }
        profile_name.setText(name);
        profile_email.setText(email);
        name = name.trim();
        String initials;
        if(name.contains(" ")) {
            String[] names = name.split(" ");
            initials = String.valueOf(names[0].charAt(0)) + String.valueOf(names[1].charAt(0));
        } else{
            if(!name.equalsIgnoreCase("")) {
                initials = String.valueOf(name.charAt(0));
            } else{
                initials = "";
            }
        }
        TextDrawable nameDrawable = TextDrawable.builder().beginConfig()
                .width(110)
                .height(110)
                .endConfig()
                .buildRound(initials, Color.BLACK);
        header_image.setImageDrawable(nameDrawable);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapterReload(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                    drawerListener.onDrawerItemSelected(view, position);
                    mDrawerLayout.closeDrawer(containerView);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    public void adapterReload(Context context){
            titles = getResources().getStringArray(R.array.drawerItems_logged_in);
            initializeView();
        adapter = new NavigationDrawerAdapter(context, getData());
        recyclerView.setAdapter(adapter);

    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().invalidateOptionsMenu();
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }


    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }



}